const MusiciansManager = artifacts.require("./MusiciansManager");

contract("MusiciansManager", (accounts) => {
  it("should add a musician", async function () {
    const Contract = await MusiciansManager.deployed();
    const result = await Contract.addMusician(
      "0xa8085ae50133e5726fad25cdb39d8c26fedc3ad3",
      "John",
      { from: accounts[0] }
    );
    assert.equal(result.logs[0].args._artistName, "John", "Not equal to John");
  });

  it("should not add musician if it already exits ", async function () {
    const Contract = await MusiciansManager.deployed();
    let err = null;
    try {
      await Contract.addMusician(
        "0xa8085ae50133e5726fad25cdb39d8c26fedc3ad3",
        "John2",
        { from: accounts[0] }
      );
    } catch (error) {
      err = error;
    }

    assert.ok(err instanceof Error);
  });
  it("should add a track", async function () {
    const Contract = await MusiciansManager.deployed();
    const result = await Contract.addTrack(
      "0x0b6aa0c4e52defeb259aaf9036cbc6ac02b05fb4",
      "jesuisperdu",
      345,
      { from: accounts[0] }
    );
    assert.equal(
      result.logs[0].args._title,
      "jesuisperdu",
      "Not equal to je suis perdu"
    );
  });
  it("should not add a track to an unknown artist ", async function () {
    const Contract = await MusiciansManager.deployed();
    let err = null;
    try {
      await Contract.addTrack(
        "0x5831ed933a386751ffa1b626647dff98b043570d",
        "Trackkpou",
        345,
        { from: accounts[0] }
      );
    } catch (error) {
      err = error;
    }

    assert.ok(err instanceof Error);
  });
});
